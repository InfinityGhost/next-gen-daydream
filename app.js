const Discord = require("discord.js")

const client = new Discord.Client({ disableMentions: "everyone" })

const commands = new Map()

const configKey = process.argv[2]

module.exports = {
    client,
    commands,
    currentCfg: { "/": "pending" },
    cfg: configKey
}

const config = require("./libraries/configManager")
const log = require("./libraries/logManager")
const quest = require("./libraries/questManager")
const event = require("./libraries/eventManager")
const command = require("./libraries/commandManager")
const db = require("./libraries/dbManager")
const web = require("./libraries/webManager")

web.init()

if (!config.configExists(configKey)) log.fatalAndExit("Config must exist", "Config")
const currentCfg = config.getConfig(configKey)

const dbOptions =
    currentCfg.db.remote ?
        { port: currentCfg.port, password: currentCfg.password, url: currentCfg.url } :
        { port: 6379, password: "", url: "localhost" }

db.getDb(dbOptions.port, dbOptions.url, { password: dbOptions.password })

quest.allCommandNames().forEach(cmd => {
    commands.set(cmd, command.getCommand(cmd))
})
quest.allEventNames().forEach(evt => {
    client.on(evt, event.getEvent(evt))
})

module.exports = {
    client,
    commands,
    currentCfg,
    cfg: configKey
}

client.login(currentCfg.token)
