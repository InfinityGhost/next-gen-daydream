const logMgr = require("../libraries/logManager")
const { client, cfg } = require("../app")
const { getConfig } = require("../libraries/configManager")
const { getActiveDb } = require("../libraries/dbManager")

module.exports = async () => {
    const currentCfg = getConfig(cfg)
    const savedActivity = await getActiveDb().get("status")
    const activity = savedActivity ? savedActivity : currentCfg.defaults.status
    logMgr.info(`Applying activity ${activity}`)

    try {
        client.user.setPresence({
            activity: {
                name: `${currentCfg.defaults.prefix}help | ${activity}`
            }
        })
    } catch (e) {
        // In case race conditions happen — unlikely but still somewhat common
        let attemptInterval = setInterval(() => {
            if (currentCfg["/"] === "pending") {
                logMgr.error(`Config not loaded — retrying in 5s (possible race condition?)`, "activity")
                return
            }

            client.user.setPresence({
                activity: {
                    name: `${currentCfg.defaults.prefix}help | ${activity}`
                }
            }).then(() => {
                logMgr.info("Clearing attempt interval", "activity")
                clearInterval(attemptInterval)
            })
        }, 5000)
    }
    logMgr.success(`Ready: ${client.user.tag}`)
}