const { client, commands, cfg } = require("../app")
const Discord = require("discord.js")
const stringSimilarity = require("string-similarity")
const config = require("../libraries/configManager")
const db = require("../libraries/dbManager")
const quest = require("../libraries/questManager")
const webevents = require("../libraries/webManager")

/**
 * @param {Discord.Message} message Message.
 */
module.exports = (message) => {
    if (message.author.bot) return

    const currentCfg = config.getConfig(cfg)
    const { permissions, product } = currentCfg

    db.getActiveDb().get(`${message.guild.id}:prefix`).then((__prefix) => {
        const prefix = __prefix ? __prefix : currentCfg.defaults.prefix

        const { author, content } = message

        if (content === `<@${client.user.id}>`) {
            return message.reply("My prefix here is `" + prefix + "`.")
        }

        if (!content.startsWith(prefix)) return

        const parts = message.cleanContent.split(/ +/)
        const command = parts[0].substr(prefix.length).toLowerCase()
        const args = parts.slice(1)

        module.exports.client = message.client
        module.exports.message = message
        module.exports.args = args
        module.exports.prefix = prefix

        if (commands.has(command)) {
            const commandFile = commands.get(command)

            if (commandFile.admin === true && !permissions.admin.includes(author.id)) {
                return message.channel.send("\\🇽 This is an admin command.")
            }

            webevents.push("CMD", `${prefix}${command} ${args.join(" ")}`)
            try {
                commandFile.fire()
            } catch (e) {
                message.react("🇽")
                webevents.push("CMD_ERR", `${prefix}${command} ${args.join(" ")} | Error: ${e}`)
            }
        } else {
            const { bestMatch } = stringSimilarity.findBestMatch(command, quest.allCommandNames())
            const embedCNF = new Discord.MessageEmbed()
                .setTitle("It seems like this command does not exist.")
                .setColor(product.color)
            if (bestMatch.rating > 0.3) {
                embedCNF
                    .setDescription(
                        `Did you mean \`${prefix}${bestMatch.target}\`?\nI am ${(
                            bestMatch.rating * 100).toFixed(2)}% sure on that.`)
            }

            webevents.push("CNF", `${prefix}${command} ${args.join(" ")}`)

            if (bestMatch.rating === 0) {
                webevents.push("CNF_INFO", `Could not figure out what ${command} means.`)
            } else {
                webevents.push("CNF_INFO", `${command} might be ${bestMatch.target} — ${
                    (bestMatch.rating * 100).toFixed(2)}% sure`)
            }

            message.channel.send(embedCNF)
        }
    })
}