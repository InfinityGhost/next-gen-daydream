const capitalizeFirstLetter = (string) => {
    if (typeof string === "undefined") return
    const firstLetter = string[0] || string.charAt(0)
    return firstLetter ? firstLetter.toUpperCase() + string.substr(1) : ''
}

module.exports = {
    capitalizeFirstLetter
}