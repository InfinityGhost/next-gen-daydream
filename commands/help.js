const { MessageEmbed } = require("discord.js")

const { cfg: configName, commands } = require("../app")

const quest = require("../libraries/questManager")
const configs = require("../libraries/configManager")

const { product } = configs.getConfig(configName)

module.exports = {
    description: "Show the command list.",
    fire() {
        const { message, prefix, args } = require("../events/message")

        const embedHelp = new MessageEmbed()
            .setColor(product.color)

        if (commands.has(args[0])) {
            const cmd = commands.get(args[0])

            embedHelp
                .setTitle(`${prefix}${args[0]}`)
                .setDescription(cmd.description)
        } else {
            embedHelp
                .setTitle(`Commands — ${product.name} v${product.version}`)
                .setDescription(`\`${quest.allCommandNames().join("` `")}\``)
                .setFooter(`Prefix in ${message.guild.name}: ${prefix} | ${prefix}help <command> for command help`)
        }

        message.channel.send(embedHelp)
    }
}