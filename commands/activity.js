module.exports = {
    description: "Change the bot's activity.",
    admin: true,
    async fire() {
        const { client, message } = require("../events/message")
        const { getActiveDb } = require("../libraries/dbManager")
        const { currentCfg } = require("../app")

        const activity = message.cleanContent.split(" ").slice(1).join(" ")
        await getActiveDb().set("status", activity)
        await client.user.setPresence({
            activity: {
                name: `${currentCfg.defaults.prefix}help | ${activity}`
            }
        })
        message.react("✅")
    }
}