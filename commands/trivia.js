const { decode } = require("he")
const fetch = require("isomorphic-unfetch")

const { MessageEmbed } = require("discord.js")

const { cfg: cfgName } = require("../app")
const { getConfig } = require("../libraries/configManager")
const { product } = getConfig(cfgName)

const { getActiveDb } = require("../libraries/dbManager")

module.exports = {
    description: "Play a game of trivia.",
    fire() {
        const { message } = require("../events/message")

        fetch("https://opentdb.com/api.php?amount=1")
            .then(res => res.json())
            .then(body => {
                const result = body.results[0]
                const answers = result.incorrect_answers.concat(result.correct_answer).sort()
                const filter = r => {
                    return decode(result.correct_answer).toLowerCase() === r.content.toLowerCase()
                }
                const embedTrivia = new MessageEmbed()
                    .setTitle(decode(result.question))
                    .setColor(product.color)
                    .addField("Category", decode(result.category))
                    .addField("Choices", decode(answers.join(", ")))
                    .setFooter("30 seconds | Type your answer here | Case insensitive")
                message.channel.send(embedTrivia).then(() => {
                    message.channel.awaitMessages(filter, {
                        max: 1,
                        time: 30 * 1000,
                        errors: ["time"]
                    }).then(collected => {
                        message.channel.send(`${collected.first().author} got it! The answer was ${result.correct_answer}. One trivia point has been added to their profile.`)
                        getActiveDb().incr(`${collected.first().author.id}:trivia`)
                    }).catch(() => {
                        message.channel.send(`Nobody got the answer this time. It was ${result.correct_answer}.`)
                    })
                })
            })
    }
}