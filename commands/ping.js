module.exports = {
    description: "Test Daydream's connection.",
    fire() {
        const { message } = require("../events/message")
        message.channel.send("⏱️").then((m) => {
            m.edit(`\\✅ Latency: ${Date.now() - m.createdTimestamp}ms`)
        })
    }
}