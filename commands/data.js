const dbModule = require("../libraries/dbManager")

const { cfg: cfgName } = require("../app")
const cfgModule = require("../libraries/configManager")

const { MessageEmbed } = require("discord.js")

const fetch = require("isomorphic-unfetch")

module.exports = {
    description: "Request a copy of your data.",
    async fire() {
        const { data, product } = cfgModule.getConfig(cfgName)
        const { message } = require("../events/message")
        const db = dbModule.getActiveDb()

        let preparedData = {}
        const keys = await db.keys(`*${message.author.id}*`)

        keys.forEach(async (key) => {
            preparedData[key] = await db.get(key)
        })

        if (data.online) {
            setTimeout(async () => {
                const res = await fetch(`${data.url}/data/push`, {
                    method: "POST",
                    body: JSON.stringify({
                        pass: data.key,
                        content: preparedData,
                        user: message.author.id
                    }),
                    headers: {
                        "content-type": "application/json"
                    }
                })
                const body = await res.json()

                const embedData = new MessageEmbed()
                    .setColor(product.color)
                    .setTitle("Data Request")
                    .setDescription(`${data.frontend}/data/${body.uniqueKey}`)
                    .setFooter("This link lasts for 5 days only")
                message.channel.send(embedData)
            }, 2000)
        } else {
            setTimeout(() => {
                message.channel.send("Your data: ```json\n" + JSON.stringify(preparedData, false, 2) + "```")
            }, 1500)
        }
    }
}