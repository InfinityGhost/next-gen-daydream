const { MessageEmbed } = require("discord.js")

module.exports = {
    description: "Show instructions on how to get the bot into your server.",
    fire() {
        const { currentCfg: { product }, client } = require("../app")
        const { message } = require("../events/message")

        const embedInviteme = new MessageEmbed()
            .setColor(product.color)
            .setTitle("Invite me")
            .setDescription(`​[Add the bot](https://discordapp.com/api/oauth2/authorize?client_id=${client.user.id
                }&permissions=8&scope=bot)\n[Server invite](https://discordapp.com/invite/${product.server})`)

        message.channel.send(embedInviteme)
    }
}