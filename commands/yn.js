const fetch = require("isomorphic-unfetch")
const { capitalizeFirstLetter } = require("../utilities/text")
const { MessageEmbed } = require("discord.js")

module.exports = {
    description: "Answer a yes/no question.",
    fire() {
        const { message } = require("../events/message")
        const { currentCfg: { product: { color } } } = require("../app")

        fetch("https://yesno.wtf/api")
            .then(res => res.json())
            .then(body => {
                const embedYn = new MessageEmbed()
                    .setTitle(capitalizeFirstLetter(body.answer))
                    .setColor(color)
                    .setImage(body.image)
                message.channel.send(embedYn)
            })
    }
}