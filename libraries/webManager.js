const DDWE = require("dd-weblib")
const { getConfig } = require("./configManager")
const { cfg: cfgName } = require("../app")

const cfg = getConfig(cfgName)

const eventClient = new DDWE(cfg.webevents.url || "", cfg.webevents.key || "")

const init = () => {
    eventClient.setActiveState(cfg.webevents.enable)
}

const push = (id, content) => {
    if (!cfg.webevents.enable) return
    return eventClient.push(id, content)
}

const get = () => {
    if (!cfg.webevents.enable) return false
    return eventClient.getAll()
}

module.exports = { eventClient, push, get, init }