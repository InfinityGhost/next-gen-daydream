const redis = require("ioredis")

let db

/**
 * @returns {redis.Redis} 
 */
const getActiveDb = () => db

const getDb = (port, host, opts) => {
    db = new redis(port, host, opts)
    return db
}

module.exports = {
    db,
    redis,
    getDb,
    getActiveDb
}