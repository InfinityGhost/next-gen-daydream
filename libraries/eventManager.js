const getEvent = (byName) => {
    return require(`../events/${byName}`)
}

module.exports = { getEvent }