const getCommand = (byName) => {
    return require(`../commands/${byName}`)
}

module.exports = { getCommand }