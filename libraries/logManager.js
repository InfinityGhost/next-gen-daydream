const logger = require("daydream-logging")

module.exports = {
    info(a, m) {
        logger.log("info", a, m)
    },
    error(a, m) {
        logger.log("error", a, m)
    },
    success(a, m) {
        logger.log("done", a, m)
    },
    debug(a, m) {
        logger.log("debug", a, m)
    },
    fatal(a, m) {
        logger.log("fatal", a, m)
    },
    log(a, m) {
        logger.log("log", a, m)
    },
    fatalAndExit(a, m) {
        logger.log("_", a, m)
        process.exit(1)
    }
}