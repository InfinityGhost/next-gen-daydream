const fs = require("fs")
const path = require("path")

const getConfig = (byName) => {
    return JSON.parse(fs.readFileSync(path.join(__dirname, "..", "config", byName + ".json")).toString())
}

const configExists = (byName) => {
    return fs.existsSync(path.join(__dirname, "..", "config", byName + ".json"))
}

const validateConfig = (config) => {
    return Object.keys(config) === Object.keys(getConfig("example"))
}

module.exports = { getConfig, configExists, validateConfig }