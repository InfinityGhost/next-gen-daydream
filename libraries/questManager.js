const fs = require("fs")
const path = require("path")

const allEventNames = () => {
    return fs.readdirSync(path.join(__dirname, "..", "events")).map(a => a.replace(/\.js$/, ""))
}

const allCommandNames = () => {
    return fs.readdirSync(path.join(__dirname, "..", "commands")).map(a => a.replace(/\.js$/, ""))
}

module.exports = {
    allEventNames,
    allCommandNames
}