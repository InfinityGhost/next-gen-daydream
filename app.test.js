const test = require('ava');

const commandService = require("./libraries/commandManager")
const questService = require("./libraries/questManager")
const expectedAdminCommands = ["activity"]

test("Expected admin commands must exist", (t) => {
    const found = questService.allCommandNames().filter(c => expectedAdminCommands.includes(c))
    t.deepEqual(found, expectedAdminCommands);
})

test('Admin commands have the admin attribute set to true', (t) => {
    const haveAdmin = []
    expectedAdminCommands.forEach(cmd => {
        if (commandService.getCommand(cmd).admin) haveAdmin.push(cmd)
    })
    t.deepEqual(haveAdmin, expectedAdminCommands);
});